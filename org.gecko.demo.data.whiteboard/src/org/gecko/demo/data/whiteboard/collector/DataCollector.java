package org.gecko.demo.data.whiteboard.collector;

import java.util.List;

import org.gecko.demo.model.demo.TLC;
import org.osgi.annotation.versioning.ProviderType;

@ProviderType
public interface DataCollector {

	List<TLC> getAllTLCs();
	
}
