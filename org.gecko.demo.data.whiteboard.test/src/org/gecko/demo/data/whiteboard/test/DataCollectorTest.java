package org.gecko.demo.data.whiteboard.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.gecko.demo.data.whiteboard.collector.DataCollector;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.osgi.test.common.annotation.InjectService;
import org.osgi.test.common.service.ServiceAware;
import org.osgi.test.junit5.cm.ConfigurationExtension;
import org.osgi.test.junit5.context.BundleContextExtension;
import org.osgi.test.junit5.service.ServiceExtension;

@ExtendWith(BundleContextExtension.class)
@ExtendWith(ServiceExtension.class)
@ExtendWith(ConfigurationExtension.class)
public class DataCollectorTest{
	

	@Nested
	class TestDefault {

		@Test
		public void default_CollectoreAvaialble(
				@InjectService(timeout = 500) ServiceAware<DataCollector> serviceAware)
				throws Exception {
			assertThat(serviceAware.getServiceReference()).isNotNull();
			DataCollector dataCollector = serviceAware.getService();
			assertThat(dataCollector).isNotNull();
		}

		@Test
		public void default_CollectoreListNotNull(
				@InjectService(timeout = 500) ServiceAware<DataCollector> serviceAware)
						throws Exception {
			DataCollector dataCollector = serviceAware.getService();
			assertThat(dataCollector.getAllTLCs()).isNotNull();
		}
	}
	
	@Test
	public void test() {
		// A Marker necessary for tests with only nested tests, until https://github.com/bndtools/bnd/pull/4521 is merged
	}
}