package org.gecko.demo.data.provider.tlc;

import java.util.LinkedList;
import java.util.List;

import org.gecko.demo.data.provider.tlc.ocd.DataProviderConfig;
import org.gecko.demo.data.whiteboard.provider.DataProvider;
import org.gecko.demo.model.demo.DemoFactory;
import org.gecko.demo.model.demo.Location;
import org.gecko.demo.model.demo.TLC;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.metatype.annotations.Designate;

@Component(name = "TLCProviderConfigRequired", configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = DataProviderConfig.class)
@DataProviderConfig(name = "Created with OCD", id = "ocd", lat = 50, lng = 12.6)
public class TLCProviderConfigRequired implements DataProvider{

	List<TLC> tlcs = new LinkedList<>();
	private String name;
	
	@Activate
	public TLCProviderConfigRequired(DataProviderConfig config) {
		name = config.name();
		tlcs.add(createTLC(config.id(), config.name(), config.lat(), config.lng()));
	}
	
	private TLC createTLC(String id, String name, Double lat, Double lng) {
		TLC tlc = DemoFactory.eINSTANCE.createTLC();
		tlc.setId(id);
		tlc.setName(name);
		Location loc = DemoFactory.eINSTANCE.createLocation();
		loc.setLat(lat);
		loc.setLng(lng);
		tlc.setLocation(loc);
		return tlc;
	}

	@Override
	public List<TLC> provideTLCs() {
		return tlcs;
	}

	@Override
	public String getName() {
		return name;
	}

}
