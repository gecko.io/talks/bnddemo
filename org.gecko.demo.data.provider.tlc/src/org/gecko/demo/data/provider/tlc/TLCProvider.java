package org.gecko.demo.data.provider.tlc;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.gecko.demo.data.whiteboard.provider.DataProvider;
import org.gecko.demo.model.demo.DemoFactory;
import org.gecko.demo.model.demo.Location;
import org.gecko.demo.model.demo.TLC;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;

@Component(property = {"name=Manuell Erstellt"})
public class TLCProvider implements DataProvider{

	List<TLC> tlcs = new LinkedList<>();
	private String name;
	
	@Activate
	private void activate(Map<String, String> props) {
		name = props.get("name");
		tlcs.add(createTLC("id1", "name1", 52.2, 11.3));
		tlcs.add(createTLC("id2", "name2", 52.2, 11.3));
		tlcs.add(createTLC("id3", "name3", 52.2, 11.3));
	}
	
	private TLC createTLC(String id, String name, Double lat, Double lng) {
		TLC tlc = DemoFactory.eINSTANCE.createTLC();
		tlc.setId(id);
		tlc.setName(name);
		Location loc = DemoFactory.eINSTANCE.createLocation();
		loc.setLat(lat);
		loc.setLng(lng);
		tlc.setLocation(loc);
		return tlc;
	}

	@Override
	public List<TLC> provideTLCs() {
		return tlcs;
	}

	@Override
	public String getName() {
		return name;
	}

}
