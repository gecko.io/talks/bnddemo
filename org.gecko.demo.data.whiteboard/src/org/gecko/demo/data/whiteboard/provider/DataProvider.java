package org.gecko.demo.data.whiteboard.provider;

import java.util.List;

import org.osgi.annotation.versioning.ConsumerType;
import org.gecko.demo.model.demo.TLC;

@ConsumerType
public interface DataProvider {
	
	List<TLC> provideTLCs();

	String getName();
	
}
