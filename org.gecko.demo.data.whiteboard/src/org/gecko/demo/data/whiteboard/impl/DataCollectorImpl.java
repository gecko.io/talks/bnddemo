package org.gecko.demo.data.whiteboard.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.gecko.demo.data.whiteboard.collector.DataCollector;
import org.gecko.demo.data.whiteboard.provider.DataProvider;
import org.gecko.demo.model.demo.TLC;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.component.annotations.ReferencePolicyOption;
import org.osgi.service.component.annotations.ServiceScope;

@Component(scope = ServiceScope.SINGLETON)
public class DataCollectorImpl implements DataCollector{

	List<DataProvider> providers = new LinkedList<DataProvider>();
	
	@Activate
	public void activate() {
		System.out.println("We don't need it, but it is nice to see that something happens");
		System.out.println("DataCollectorImpl activated");
	}

	@Deactivate
	public void deactivate() {
		System.out.println("DataCollectorImpl deactivated");
	}
	
	@Override
	public List<TLC> getAllTLCs() {
		if(providers.isEmpty()) {
			return null;
		}
		List<TLC> result = new LinkedList<TLC>();
		providers.stream().map(DataProvider::provideTLCs).forEach(result::addAll);
		return result;
	}
	
	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC, policyOption = ReferencePolicyOption.GREEDY)
	public void addProvider(DataProvider provider, Map<String, String> props) {
		System.out.println(String.format("Adding DataProvider with internalName %s and configuredName %s", provider.getName(), props.get("name")));
		providers.add(provider);
	}
	
	public void removeProvider(DataProvider provider, Map<String, String> props) {
		System.out.println(String.format("Removing DataProvider with internalName %s and configuredName %s", provider.getName(), props.get("name")));
		providers.remove(provider);
	}
}
