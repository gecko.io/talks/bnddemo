/**
import org.osgi.service.jaxrs.whiteboard.annotations.RequireJaxrsWhiteboard; * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.demo.rest.tlc;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.gecko.demo.data.whiteboard.collector.DataCollector;
import org.gecko.demo.model.demo.DemoFactory;
import org.gecko.demo.model.demo.TLCResponse;
import org.gecko.emf.osgi.rest.jaxrs.annotation.RequireEMFMessageBodyReaderWriter;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;
import org.osgi.service.http.whiteboard.annotations.RequireHttpWhiteboard;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsName;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsResource;

/**
 * This is a Demo Resource for a Jaxrs Whiteboard 
 * 
 * @since 1.0
 */
@RequireHttpWhiteboard
@RequireEMFMessageBodyReaderWriter
@JaxrsResource
@JaxrsName("tlc")
@Component(service = TLCResource.class, scope = ServiceScope.PROTOTYPE)
@Path("/tlc")
public class TLCResource {
	
//	@Reference
//	private DataCollector collector;
//	
//	@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response get() {
//		TLCResponse result = DemoFactory.eINSTANCE.createTLCResponse();
//		result.getTlcs().addAll(collector.getAllTLCs());
//		return Response.ok(result).build();
//	}
	
	@GET
	@Path("/hello")
	public String hello() {
		return "hello World";
	}

}
