package org.gecko.demo.data.whiteboard.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collections;
import java.util.List;

import org.gecko.demo.data.whiteboard.collector.DataCollector;
import org.gecko.demo.data.whiteboard.provider.DataProvider;
import org.gecko.demo.model.demo.DemoFactory;
import org.gecko.demo.model.demo.TLC;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.osgi.framework.BundleContext;
import org.osgi.test.common.annotation.InjectBundleContext;
import org.osgi.test.common.annotation.InjectService;
import org.osgi.test.common.dictionary.Dictionaries;
import org.osgi.test.common.service.ServiceAware;
import org.osgi.test.junit5.cm.ConfigurationExtension;
import org.osgi.test.junit5.context.BundleContextExtension;
import org.osgi.test.junit5.service.ServiceExtension;

@ExtendWith(BundleContextExtension.class)
@ExtendWith(ServiceExtension.class)
@ExtendWith(ConfigurationExtension.class)
public class DataProviderTest{
	
	@BeforeAll
	public static void beforeAll(@InjectBundleContext BundleContext bundleContext) throws Exception {
		bundleContext.registerService(DataProvider.class, new DataProvider() {

			@Override
			public List<TLC> provideTLCs() {
				TLC tlc = DemoFactory.eINSTANCE.createTLC();
				tlc.setId("testId");
				tlc.setName("testName");
				return Collections.singletonList(tlc);
			}

			@Override
			public String getName() {
				return "anotherTestName";
			}}, Dictionaries.dictionaryOf("name", "test"));
	}
	
	@Test
	public void providerWorks(
			@InjectService ServiceAware<DataCollector> collector,
			@InjectService( filter = "(name=test)") ServiceAware<DataProvider> providerAware)
					throws Exception {
		DataProvider dataProvider = providerAware.waitForService(500);
		assertThat(dataProvider).isNotNull();
		
		DataCollector dataCollector = collector.getService();
		assertThat(dataCollector.getAllTLCs())
			.isNotNull()
			.isNotEmpty();
		TLC c = dataCollector.getAllTLCs().get(0);
		assertThat(c.getId()).isEqualTo("testId");
		assertThat(c.getName()).isEqualTo("testName");
	}
	
}