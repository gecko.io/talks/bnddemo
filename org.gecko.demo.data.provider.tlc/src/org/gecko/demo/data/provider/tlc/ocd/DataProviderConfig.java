package org.gecko.demo.data.provider.tlc.ocd;

import org.osgi.service.component.annotations.ComponentPropertyType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ComponentPropertyType
@ObjectClassDefinition
public @interface DataProviderConfig {

	String name();
	String id();
	double lat();
	double lng();
	
}
